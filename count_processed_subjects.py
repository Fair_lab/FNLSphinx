#! /usr/global/bin/python
import os
import argparse

def main():
    parser = get_parser()

    args = parser.parse_args()
    proc_study_path = args.study_dir

    num_subjects = get_count()

    print "number of subjects is {}".format(num_subjects)

def get_parser():

    parser = argparse.ArgumentParser(description="HCP one click node runner")
    parser.add_argument('-s', metavar='STUDY_DIR', action='store', required=True, type=os.path.abspath,
                            help='Absolute path to processed study dir.',
                            dest='study_dir')
    return parser


def get_count(study_dir):
    """ Get the number of processed subjects in a directory

    This is secretly just counting the number of things in a directory

    Args:
        study_dir (str): absolute path to study folder containing processed subject data
    Returns:
        count (int): number of subjects that have been processed in the study 
    """

    return len(os.listdir(study_dir))
    

if __name__ == "__main__":
    main()