# FNL Sphinx Documentation Tutorial/Demo

### Goals for today:

1. What's this Sphinx thing anyway?
2. Demo: Sphinx setup for a simple project on the AIRC
3. Demo: deploy Sphinx documentation on GitLab pages
4. Showcase: hcponeclick documentation

### Format:
- I'll demo, but interrupt at any time if you have questions
- If time/interest: informal discussion at the end about documentation standards

#### If you want to follow along:
 - You can follow along on your laptop, but we might not have time to troubleshoot during this session.
 - You'll have access to this whole tutorial and can repeat anything in this tutorial at a later time if you want:
  - https://gitlab.com/Fair_lab/FNLSphinx
  - You can see what your code should look like after the tutorial is complete by checking out the pagesExample branch: https://gitlab.com/Fair_lab/FNLSphinx/tree/pagesExample
 - The tutorial expects you to have terminal access to the AIRC, but you can do these things on other systems
 - The tutorial's commands are geared towards mac/linux users.
   - You can try using [git-bash](https://git-scm.com/download/win) on windows
 - I'm happy to help people one-on-one later if they run into issues/need help with configuration for their system :-). 
   Just shoot me an email at waltonwe@ohsu.edu

### Caveat: I'm not a Sphinx expert
![not an expert](http://memesvault.com/wp-content/uploads/I-Have-No-Idea-What-Im-Doing-Dog-02.jpg)

-----------------------------------------------------------------------------------------
## What's this Sphinx thing anyway?

1. Documentation tool for Python, written in Python.
 - http://www.sphinx-doc.org/en/stable/
 - sometimes used to document non-Python [things](http://www.sphinx-doc.org/en/stable/examples.html#homepages-and-other-non-documentation-sites)
2. Generates html, LaTeX, PDF...
3. Can turn your in-code function docblocs into formatted html
4. Uses a markdown-like code called reStructuredText


### Why do *we* care?

1. repeat #3
2. Documentation is arguably more readable and definitely more automated than our current wiki setup
3. Deploy docs to public GitLab pages for each project. Free hosting!

### Things to consider before jumping on the Sphinx train
1. Sphinx doesn't do a great job at pulling code documentation from other languages
 - has support for C/C++, but AFAIK that's it
 - there are usually other packages that help with that: Javadoc, PHPdoc
2. [insert the usual debates about code documentation here]
 - Let's save this debate for the end


-----------------------------------------------------------------------------------------
## Sphinx setup

### virtualenv
If you're fortunate enough to have sufficient privileges on your system to install Python packages and don't care about the AIRC, skip to [Upgrade Sphinx and install packages](#upgrade-sphinx-and-install-packages)

![virtualenvs are scary](http://i.giphy.com/Zt2f45vHLZtaU.gif)

Ok, this is just *one* way to get Sphinx set up. With our current situation on the AIRC, using virtualenv is easiest way to get started. I've been able to install and run Sphinx on my linux box at home (I have sudo privileges) and on my Windows machine at work (after updating pip). I found out the hard way that Python libraries I sometimes use aren't available on Windows(fcntl, pwd), so I think it's best to run Sphinx on whatever OS you're writing your code for. 

**It's really not *that* bad.** 

Just execute the following on the AIRC command prompt:
```
#go to your home directory.
cd ~ 
mkdir Sphinx

#init your virtualenv
virtualenv --system-site-packages Sphinx

#start using your virtualenv
source ~/Sphinx/bin/activate

```
Now your command prompt should look like:
```
(Sphinx)you@beast:~$
```
You can activate and deactivate this virtualenv whenever you want (i.e. if you need to 
get back to the standard beast Python configuration):
```
# stop using your Sphinx virtualenv:
deactivate
# start using your Sphinx virtualenv just like before:
source ~/Sphinx/bin/activate
```


More on [virtualenvs](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

### Upgrade Sphinx and install packages
The AIRC has Sphinx, but it's a crusty old version. We're going to install a new version and
a few friends, so execute the following on the AIRC command line:

```
pip install --upgrade sphinx
pip install sphinxcontrib-napoleon
pip install sphinx-argparse
```
If you're on a system that doesn't have sphinx, run ```pip install sphinx``` instead of ```pip install --upgrade sphinx```
This takes some time, so while that's going on, let's look at our example script: count_processed_subjects.py.

### Initialize project documentation

**If you're using FNLSphinx as a sandbox, at this point you'll want to fork your own copy via [GitLab](https://gitlab.com/Fair_lab/FNLSphinx) and
clone it wherever you work on projects.**

Sphinx has a handy tool called ```sphinx-quickstart``` you can use to help you get your 
documentation set up

1. cd into your project directory(mine's ~/repos/FNLSphinx), and checkout a new branch:

 ```
    cd ~/repos/FNLSphinx
    git checkout -b pages
 ```
2. run sphinx-quickstart:

 ```
    sphinx-quickstart
 ```
 - **READ THE SCRIPT'S OUTPUT** so you know what's going on
 - Most of the defaults are fine, but make sure to enter the following:
    - ```> Root path for the documentation [.]:``` docs
    - ```> autodoc: automatically insert docstrings from modules (y/n) [n]:``` y
    - There are more things you can play with here, but this is all you need
3. Inspect the results! You should see a new ```docs``` directory in your project

### Update your Sphinx configuration file

Open ```docs/config.py``` for editing:

1. Add your project directory to the path

 - Add to the top of the file:
 
 ```
 import sys, os
 sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
 ```
 - ![NO](http://i.giphy.com/vDurI6FYH7qi4.gif)

   Generally, having to manually update your Python path is a
   sign that you're doing something wrong--but this is an exception.
2. Add the packages we downloaded to the ```extensions``` list:

 ```
 extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinxarg.ext'
 ]
 ```
3. (Optional) Update the html_theme:
 - Latest Sphinx defaults to the 'alabaster' theme
 - I set mine to 'default'
 - Other built-in themes: http://www.sphinx-doc.org/en/stable/theming.html#builtin-themes
 - Make your own theme: http://www.sphinx-doc.org/en/stable/theming.html#creating-themes

### Build and inspect your bare-bones docs

```
cd docs
make html
```

open ```docs/_build/html/index.html``` using the browser of your choice

```
firefox _build/html/index.html
```

### Your first reStructuredText file
open docs/index.rst

pull in your function and argparse documentation by adding this code:
```
How to run count_processed_subjects.py:
---------------------------------------
.. argparse::
   :module: count_processed_subjects
   :func: get_parser
   :prog: ./count_processed_subjects.py

.. toctree::
   :maxdepth: 2

Functions:
----------
.. automodule:: count_processed_subjects
    :members:

```
This is where the napoleon, autodoc, and argparse extension come in.
- autodoc: pulls code documentation into reStructuredText
- napolean: lets you use google style docstrings. These are [much more readable](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/) than
  the default Sphinx docStrings 
- argparse: Generates documentation from your argparse code


### Build and inspect your docs again

Same as before:

```
cd docs
make html
firefox _build/html/index.html
```

![Yayyy!!!](http://i.giphy.com/LZElUsjl1Bu6c.gif)

-----------------------------------------------------------------------------------------
## Deploy Docs to GitLab
GitLab gives you a free web page to go along with your GitLab repo on 'gitlab.io'.
 GitLab also has it's own CI(Continuous Integration) server you can leverage to do things like automate 
builds or trigger automated tests.

#### You need to dip your toes into the CI pool to get your Sphinx docs deployed to gitlab.io.

![NOPE](http://i.giphy.com/6h4z4b3v6XWxO.gif)

**I promise it's not scary! You only have to do the setup ONE TIME:**

1. In your the root of your project directory, create a file called ```.gitlab-ci.yml```
2. Add the following snippet to that file (and save):

```
pages:
  script:
  - rm -rf public
  - mv docs/_build/html public
  artifacts:
    paths:
    - public
  only:
  - pages
```
 3. Commit everything to your pages branch, and push your branch to GitLab:
 
    ```
    cd ~/repos/FNLSphinx
    git add -A
    git add -A docs
    git commit -m 'built and deployed first documentation page'
    git push origin pages:pages
    ```
 4. Open the project in GitLab and look in the 'Builds' tab. For me, it's https://gitlab.com/alexwweston/FNLSphinx/builds.
  - click on the 'BuildID' if you're curious about what gitlab-ci did/is doing
 5. Go to http://group_or_uname.gitlab.io/your_project/. For me, it's http://alexwweston.gitlab.io/FNLSphinx/

![WOOOOOO!](http://i.giphy.com/4T48716LEWUGA.gif)

Every time you push changes to the pages branch, your latest documentation will be automatically deployed!

-----------------------------------------------------------------------------------------
## Tour of hcponeclick docs

If there's time, let's do a quick tour what I was able to do for the hcponeclick docs.

- docs: http://fair_lab.gitlab.io/hcponeclick/
- code: https://gitlab.com/Fair_lab/hcponeclick/tree/pages/docs

-----------------------------------------------------------------------------------------
## Resources:
Sphinx:
- Sphinx homepage: http://www.sphinx-doc.org/en/stable/index.html
- Sphinx getting started: http://www.sphinx-doc.org/en/stable/tutorial.html
  - also explains reStructuredText directives (things like ```..toctree::```)
- intro to reStructuredText: http://www.sphinx-doc.org/en/stable/rest.html
- napolean: https://sphinxcontrib-napoleon.readthedocs.io/en/latest/
- autodoc: http://www.sphinx-doc.org/en/stable/ext/autodoc.html
- http://sphinx-argparse.readthedocs.io/en/latest/
- Sphinx full code example: https://pythonhosted.org/an_example_pypi_project/sphinx.html#full-code-example

GitLab:
- pages: http://doc.gitlab.com/ee/pages/README.html
- GitLab CI: https://about.gitlab.com/gitlab-ci/

Misc:
- How to use quick documentation to bring up docblocs in PyCharm: https://www.jetbrains.com/help/pycharm/2016.1/documentation-tool-window.html?origin=old_help